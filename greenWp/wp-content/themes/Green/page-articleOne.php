<?php 
  /* 
	 Template Name: Template for the main page
  */
 ?>
 <!DOCTYPE html>
<html <?php language_attributes();?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
     <?php wp_head();
      the_post(); ?>
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,800|Roboto+Slab:400,700|Roboto:400,700,700i&display=swap" rel="stylesheet" />
     <link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500i&display=swap" rel="stylesheet">
    <!-- HTML5 shim and Respond.js-->
    <!-- [if lt IE 9]>
		<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
	<! [endif]-->
  </head>
  <body>
    <header class="wrapper header">
      <img src="<?php green_img('/img/svg/LogoHeaderFirstArcticle.svg'); ?>" alt="Logotype" class="logoHeaderFirstArticle" />
    </header>
    <div class="article wrapper">
      <h1 class="article__h"> <?php the_title(); ?></h1>
      <div class="article__a">
        <a href="https://www.facebook.com/"><img src="<?php green_img('/img/svg/svgGreenFacebook.svg'); ?>" alt="iconFacebook" /></a>
        <a href="https://twitter.com/"><img src="<?php green_img('/img/svg/svgGreenTwitter.svg'); ?>" alt="iconTwitter" /></a>
        <a href="http://1629939.dissmay.web.hosting-test.net/" class="iconCopy"><img src="<?php green_img('/img/svg/svgGreenCopy.svg'); ?>" alt="iconCopy " /></a>
      </div>
    </div>
    <p class="article__date wrapper"><?php the_author()?> | <?php the_date()?></p>
    <article class="mainContent wrapper  ">
    	 <?php the_content(); ?>
    </article>






    
    <footer class="footerFooterFirstArticle">
      <?php echo do_shortcode('[contact-form-7 id="182" title="Контактная форма 1" html_class="wrapper"]'); ?>

     <!--  <form action="#" class="form wrapper">
        <p class="form__with_p"> Stay in Touch! </p>
        <div class="form__divInput">
          <input type="text" class="form__input" placeholder="Full name" />
          <input type="text" class="form__input" placeholder="Phone" />
          <input type="text" class="form__input" placeholder="Email" />
          <button class="form__buttonSubmit">Join us</button>
        </div>
      </form> -->

    
      
      <div class="footerFooterFirstArticle__footerNav wrapper">
        <img src="<?php green_img('/img/svg/LogoFooterFirstArticle.svg'); ?>" alt="LogoFooter" />
        <nav>
          <div class="footerFooterFirstArticle__a">
            <a href="https://www.facebook.com/"><img src="<?php green_img('/img/svg/svgWhiteFacebook.svg'); ?>" alt="iconFacebook" /></a>
            <a href="https://twitter.com/"><img src="<?php green_img('/img/svg/svgWhiteTwitter.svg'); ?>" alt="iconTwitter" /></a>


            <a href="http://1629939.dissmay.web.hosting-test.net/"  class="iconCopyTwo"><img src="<?php green_img('/img/svg/svgWhiteCopy.svg'); ?>" alt="iconCopy" /></a>
          </div>
        </nav>
      </div>
      <div class="footerFooterFirstArticle__footerCorp">
        <p>(c) GreenInvestment | All rights reserved</p>
      </div>
    </footer>

      <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/1.5.10/clipboard.min.js"></script>
    <?php wp_footer(); ?>

  </body>
</html>
